import math
import sys
file_input=str(sys.argv[1])

file_testing=str(sys.argv[2])
#open the model file
model = open(file_input,'r',errors='ignore')
#open the output file 

#open the input file
input = open(file_testing,'r',errors='ignore')
#create a dictionary to put the probability for each class
prob_dict={}
class_prob_dict={}
class_name_dict={}
words_prob={}
#read each line in the input file and store in a list 'input_words'
input_words=[]
class_count=0
for line in model:
 if line.startswith("**CLASS_PROPERTIES**:"):
  words=line.split()
  class_name_dict[class_count]=words[1]
  class_prob_dict[class_count]=words[2]
  words_prob[class_count]={}
  class_count+=1
  line=next(model)
 l=line.split()
 words_prob[class_count-1][l[0]]=(l[1])
prob_res_class={}
for line in input:
 words=line.split()
 prob_res={}
 for i in class_name_dict:
  for w in words:
    if i not in prob_res:
     prob_res[i]=math.log(float(class_prob_dict[i]))
    if w in words_prob[i]:
     prob_res[i]+=math.log(float(words_prob[i][w]))
    if w not in words_prob[i]:
     prob_res[i]+=math.log(float(words_prob[i]['unknown_divya_swaminathan_not_possible'])) 
 result=max(prob_res,key=prob_res.get)
 print(class_name_dict[result])
 


