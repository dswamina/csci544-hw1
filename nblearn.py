import sys
import errno
from collections import Counter
file_input=str(sys.argv[1])
file_output=str(sys.argv[2])
#path = '/home/divyaswaminathan/Desktop/training.txt'
# 0 - Class Label-0 , 1 - Class Label - 1
class_dict={}
d=0
total_count=0
model = open(file_output,'w',errors='ignore')
with open(file_input,'r',errors='ignore') as tr:
 for line in tr:
  total_count=total_count+1
  words=line.split()
  if words[0] not in class_dict.values():
   class_dict[d]=words[0]
   d=d+1
  else:
   continue
#print(class_dict)
# 0 - Class Label0 Count , 1 - Class Label1 Count
count_dict={}
with open(file_input,'r',errors='ignore') as tr:
 for line in tr:
  words=line.split()
  for i in class_dict:
   if words[0]==class_dict[i]:
    if i in count_dict:
     count_dict[i]+=1
    else:
     count_dict[i]=1

#print(count_dict)   
#print(count_dict) - Count value for each class of words 
#define dictionary within dictionary and perform computation 
class_word_dict={}
distinct_words=0
with open(file_input,'r',errors='ignore') as tr:
 words_c=Counter(tr.read().split())
 for i in words_c.items():
  distinct_words+=1
print(distinct_words) 
#print (class_word_dict)

with open(file_input,'r',errors='ignore') as tr:
 for line in tr:
  words=line.split()
  for i in class_dict:
   if words[0]==class_dict[i]:
    for w in words[1:]:
     if i not in class_word_dict:
      class_word_dict[i]={}
     if w in class_word_dict[i]:
      class_word_dict[i][w]+=1
     if w not in class_word_dict[i]:
      class_word_dict[i][w]=1
for i in class_dict:
 class_word_dict[i]['unknown_divya_swaminathan_not_possible']=1

#print (class_word_dict[0])
total_word_count=0
#total_words in vocabulary 
for i in count_dict:
 total_word_count=total_word_count + count_dict[i]
#print (total_word_count)
count=0
#count the number of words in each class 
word_count_each_class={}
for i in class_dict:
 for j in class_word_dict[i]:
  count+=class_word_dict[i][j]
 word_count_each_class[i]=count
 count=0
print(word_count_each_class)
#probability for every word in a class
prob_word_dict={}
for i in class_word_dict:
 prob_word_dict[i]={}
# len1 = len(class_word_dict[i])
 len1=distinct_words
#total under each class
 len2 = word_count_each_class[i]
 for w in class_word_dict[i]:
  prob_word_dict[i][w]=class_word_dict[i][w]/(len1+len2)
prob_class_dict={}
for i in class_dict:
 prob_class_dict[i]=count_dict[i]/total_count  
for i in class_dict:
 model.write("**CLASS_PROPERTIES**: ")
 model.write(class_dict[i])
 model.write(" ")
 model.write(str(prob_class_dict[i]))
 model.write("\n")
 #for j in class_word_dict:
 for w,k in class_word_dict[i].items():
   #model.write(class_dict[i])
   #print(class_dict[i])
  model.write(" ")
  model.write(w)
  model.write(" ")
  #model.write(str(k))
  # print(w,k)
  model.write(" ")
   #print(" ")
  model.write(str(prob_word_dict[i][w]) )
  # print(prob_word_dict[j][w])
  model.write("\n")
 #print("\n")
#model.write('UNKNOWNBLAHBLAHDIVYA')
#model.write(" ")
#temp=float(1/(distinct_words)+word_count_each_class[i])
#model.write(str(temp))
